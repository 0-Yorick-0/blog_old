<?php

namespace App\Tests\Controller;

use Symfony\Component\Panther\PantherTestCase;

class HomeControllerTest extends PantherTestCase
{
    public function testHomePage(): void
    {
        $client = static::createPantherClient([
            'browser' => PantherTestCase::FIREFOX,
        ]);

        $crawler = $client->request('GET', '/');
        $this->assertSelectorTextContains('p', 'This is Home, for now.');
    }
}
